QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
#解决MSVC中文乱码
msvc {
    QMAKE_CFLAGS += /utf-8
    QMAKE_CXXFLAGS += /utf-8
}
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    libqrencode/bitstream.c \
    libqrencode/mask.c \
    libqrencode/mmask.c \
    libqrencode/mqrspec.c \
    libqrencode/qrencode.c \
    libqrencode/qrinput.c \
    libqrencode/qrspec.c \
    libqrencode/rsecc.c \
    libqrencode/split.c \
    main.cpp \
    mainwindow.cpp \
    qrgenerator.cpp

HEADERS += \
    libqrencode/bitstream.h \
    libqrencode/config.h \
    libqrencode/mask.h \
    libqrencode/mmask.h \
    libqrencode/mqrspec.h \
    libqrencode/qrencode.h \
    libqrencode/qrencode_copy.h \
    libqrencode/qrencode_inner.h \
    libqrencode/qrinput.h \
    libqrencode/qrspec.h \
    libqrencode/rsecc.h \
    libqrencode/split.h \
    mainwindow.h \
    qrgenerator.h

#QZxing
include(./qzxing/src/QZXing.pri)
INCLUDEPATH += ./qzxing/src

FORMS += \
    mainwindow.ui
#libqrencode使用的配置文件
DEFINES +=HAVE_CONFIG_H
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    libqrencode/config.h.in
