﻿#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setWindowTitle("二维码工具\t香菇滑稽\tWX：784765727");
    w.show();
    return a.exec();
}
