﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThreadPool>
#include <QPalette>
#include "qrgenerator.h"
#include "QZXing.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QStringList batchGenSN(quint64 startNum, quint64 quantity);
    void updateProgress();
private slots:
    void on_btn_batchGenerate_clicked();

    void on_btn_generate_clicked();

    void on_btn_saveFile_clicked();

    void on_btn_selectFile_clicked();

    void on_cBox_qrsize_stateChanged(int arg1);

    void on_btn_selqrcolor_clicked();

    void on_cBox_qrcolor_stateChanged(int arg1);

    void on_btn_selbgcolor_clicked();

    void on_cBox_bgcolor_stateChanged(int arg1);

    void on_btn_savePath_clicked();

private:
    Ui::MainWindow *ui;
    quint64 progressValue;
    enum DecoderFormat {
        DecoderFormat_None = 0,
        DecoderFormat_Aztec = 1 << 1,
        DecoderFormat_CODABAR = 1 << 2,
        DecoderFormat_CODE_39 = 1 << 3,
        DecoderFormat_CODE_93 = 1 << 4,
        DecoderFormat_CODE_128 = 1 << 5,
        DecoderFormat_DATA_MATRIX = 1 << 6,
        DecoderFormat_EAN_8 = 1 << 7,
        DecoderFormat_EAN_13 = 1 << 8,
        DecoderFormat_ITF = 1 << 9,
        DecoderFormat_MAXICODE = 1 << 10,
        DecoderFormat_PDF_417 = 1 << 11,
        DecoderFormat_QR_CODE = 1 << 12,
        DecoderFormat_RSS_14 = 1 << 13,
        DecoderFormat_RSS_EXPANDED = 1 << 14,
        DecoderFormat_UPC_A = 1 << 15,
        DecoderFormat_UPC_E = 1 << 16,
        DecoderFormat_UPC_EAN_EXTENSION = 1 << 17,
        DecoderFormat_CODE_128_GS1 = 1 << 18
    } ;
};
#endif // MAINWINDOW_H
