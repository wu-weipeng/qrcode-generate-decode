﻿#ifndef QRGENERATOR_H
#define QRGENERATOR_H

#include <QObject>
#include <QThread>
#include <QRunnable>
#include <QRect>
#include <QPixmap>
#include <QPainter>
#include <QColor>
#include"libqrencode/qrencode.h"
class QRgenerator : public QObject, public QRunnable
{
    Q_OBJECT
public:
    explicit QRgenerator(QObject *parent = nullptr);
    explicit QRgenerator(QObject *parent = nullptr,
                         QString content = "hello world!",
                         QString name = "",
                         QRect size = QRect(0,0,640,640),
                         QColor color = Qt::black,
                         QColor bgColor = QColor(255,255,255,255));
    void setContent(QString);
    void setSize(QRect);
    void setQRColor(QColor);
    void setBGColor(QColor);
    void setSaveName(QString);
    QImage GenerateQImage();
    QPixmap GenerateQPixmap();
    void saveToFile();

    void run() override;

signals:
    void finishGen();
private:
    QString qrContent;//二维码内容，默认hello world!
    QString saveName;//二维码图片保存路径，完整路径
    QRect imgSize;//生成图片大小，默认640x640
    QColor qrColor;//二维码颜色，默认黑色
    QColor backgroundColor;//背景颜色，默认白色
};

#endif // QRGENERATOR_H
